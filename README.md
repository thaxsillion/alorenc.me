## Website

From within the `website/` directory run:

```npm run build```

This will build the website to the `dist` directory.

## CICD

The process is automated using GitLab pipelines - see into the `.gitlab-ci.yml` file.
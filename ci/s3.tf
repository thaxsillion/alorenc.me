data "aws_iam_policy_document" "alorenc_me_s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["arn:aws:s3:::alorenc.me/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access.iam_arn]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = ["arn:aws:s3:::alorenc.me"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access.iam_arn]
    }
  }
}

resource "aws_s3_bucket" "s3_alorenc_me" {
  bucket = "alorenc.me"
  acl    = "public-read"
  policy = data.aws_iam_policy_document.alorenc_me_s3_policy.json

  website {
    index_document = "index.html"
  }

  versioning {
    enabled = true
  }
}

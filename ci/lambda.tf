
data "archive_file" "lambda_edge_alorenc_me" {
  type        = "zip"
  source_file = "${path.module}/assets/lambda_edge_alorenc_me.js"
  output_path = "${path.module}/assets/lambda_edge_alorenc_me.zip"
}

data "archive_file" "lambda_edge_alorenc_me_origin_request" {
  type        = "zip"
  source_file = "${path.module}/assets/lambda_edge_alorenc_me_origin_request.js"
  output_path = "${path.module}/assets/lambda_edge_alorenc_me_origin_request.zip"
}

resource "aws_lambda_function" "lambda_edge_alorenc_me" {
  function_name = "lambda-edge-alorenc-me"
  role          = aws_iam_role.lambda_edge_alorenc_me_exec.arn
  handler       = "lambda_edge_alorenc_me.handler"
  provider      = aws.lambda_edge

  filename         = data.archive_file.lambda_edge_alorenc_me.output_path
  source_code_hash = data.archive_file.lambda_edge_alorenc_me.output_base64sha256

  runtime = "nodejs12.x"
  publish = true
}

resource "aws_lambda_function" "lambda_edge_alorenc_me_origin_request" {
  function_name = "lambda-edge-alorenc-me-origin-request"
  role          = aws_iam_role.lambda_edge_alorenc_me_exec.arn
  handler       = "lambda_edge_alorenc_me_origin_request.handler"
  provider      = aws.lambda_edge

  filename         = data.archive_file.lambda_edge_alorenc_me_origin_request.output_path
  source_code_hash = data.archive_file.lambda_edge_alorenc_me_origin_request.output_base64sha256

  runtime = "nodejs12.x"
  publish = true
}

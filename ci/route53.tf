
locals {
  zone_name = "alorenc.me."
  zone_id   = "Z3SAMVIUNZAAFD"
}

resource "aws_route53_record" "www_alorenc_me_a" {
  zone_id = local.zone_id
  name    = "www.${local.zone_name}"
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.alorenc_me_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.alorenc_me_distribution.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "alorenc_me_a" {
  zone_id = local.zone_id
  name    = local.zone_name
  type    = "A"

  alias {
    name                   = aws_cloudfront_distribution.alorenc_me_distribution.domain_name
    zone_id                = aws_cloudfront_distribution.alorenc_me_distribution.hosted_zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "acm-validation-2" {
  zone_id = local.zone_id
  name    = "lg4vh4idermx36z5m4iswkqqsdmotdgk._domainkey.alorenc.me."
  type    = "CNAME"
  ttl     = "1800"

  records = ["_4270b48de79462f1ba7808707aa2bb3a.ltfvzjuylp.acm-validations.aws."]
}

resource "aws_route53_record" "google-site-verification-1" {
  zone_id = local.zone_id
  name    = local.zone_name
  type    = "TXT"
  ttl     = "300"

  records = ["google-site-verification=IxD71ZLOWEdyOfzag5k8jI064AWw9ODBCbz0FmIRTNg"]
}

resource "aws_route53_record" "mx-google" {
  zone_id = local.zone_id
  name    = local.zone_name
  type    = "MX"
  ttl     = "1800"

  records = [
    "1 ASPMX.L.GOOGLE.COM.",
    "5 ALT1.ASPMX.L.GOOGLE.COM.",
    "5 ALT3.ASPMX.L.GOOGLE.COM.",
    "10 ALT3.ASPMX.L.GOOGLE.COM.",
    "10 ALT4.ASPMX.L.GOOGLE.COM."
  ]
}

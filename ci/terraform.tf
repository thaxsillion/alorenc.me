terraform {
  backend "s3" {
    bucket = "aws-alorenc-terraform-bucket"
    key    = "terraform/dev/terraform_dev.tfstate"
    region = "eu-west-1"
  }
}

resource "aws_cloudfront_origin_access_identity" "origin_access" {
  # (resource arguments)
}

resource "aws_cloudfront_distribution" "alorenc_me_distribution" {
  origin {
    domain_name = aws_s3_bucket.s3_alorenc_me.bucket_regional_domain_name
    origin_id   = "S3-www.alorenc.me"

    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.origin_access.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "alorenc.me cf distribution"
  default_root_object = "index.html"

  custom_error_response {
    error_code            = 404
    response_code         = 200
    response_page_path    = "/error-pages/index.html"
    error_caching_min_ttl = 0
  }

  aliases = ["alorenc.me", "www.alorenc.me"]

  price_class = "PriceClass_100"

  tags = {
    Environment = "production"
  }

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "S3-www.alorenc.me"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000

    lambda_function_association {
      event_type   = "viewer-response"
      lambda_arn   = "${aws_lambda_function.lambda_edge_alorenc_me.arn}:${aws_lambda_function.lambda_edge_alorenc_me.version}"
      include_body = false
    }

    lambda_function_association {
      event_type   = "origin-request"
      lambda_arn   = "${aws_lambda_function.lambda_edge_alorenc_me_origin_request.arn}:${aws_lambda_function.lambda_edge_alorenc_me_origin_request.version}"
      include_body = false
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    minimum_protocol_version = "TLSv1.1_2016"
    acm_certificate_arn      = "arn:aws:acm:us-east-1:791916177569:certificate/5e3002c1-0b1e-4f0d-af53-e82d1223a027"
    ssl_support_method       = "sni-only"
  }
}

output "cf_distribution_id" {
  value = aws_cloudfront_distribution.alorenc_me_distribution.id
}
'use strict';

exports.handler = (event, context, callback) => {
    const request = event.Records[0].cf.request;
    const oldUri = request.uri;

    const newUri = oldUri.replace(/\/$/, '\/index.html');
    request.uri = newUri;

    return callback(null, request);
};

'use strict';

exports.handler = (event, context, callback) => {
    const response = event.Records[0].cf.response;
    const headers = response.headers;

    headers['strict-transport-security'] = [{
        key: 'Strict-Transport-Security',
        value: 'max-age=63072000; includeSubdomains; preload'
    }];

    headers['content-security-policy'] = [{
        key: 'Content-Security-Policy',
        value: "default-src 'none';" + 
        "upgrade-insecure-requests;" +
        "base-uri 'self';" +
        "img-src 'self';" +
        "font-src fonts.googleapis.com fonts.gstatic.com;" +
        "script-src 'self' 'sha256-rBFr4vTmUiTXF0BNHTsDX6zGs1c0DB7EA+H/BsRSbvY=' www.googletagmanager.com use.fontawesome.com;" +
        "style-src 'self' 'sha256-bviLPwiqrYk7TOtr5i2eb7I5exfGcGEvVuxmITyg//c=' fonts.googleapis.com;"
    }];

    headers['feature-policy'] = [{key: 'Feature-Policy', value: 
        "feature-policy: layout-animations 'none'; unoptimized-images 'none'; oversized-images 'none'; sync-script 'none'; sync-xhr 'none'; unsized-media 'none';"
    }];

    headers['x-content-type-options'] = [{key: 'X-Content-Type-Options', value: 'nosniff'}]; 
    headers['x-frame-options'] = [{key: 'X-Frame-Options', value: 'DENY'}]; 
    headers['x-xss-protection'] = [{key: 'X-XSS-Protection', value: '1; mode=block'}]; 
    headers['referrer-policy'] = [{key: 'Referrer-Policy', value: 'same-origin'}]; 

    callback(null, response);
};
